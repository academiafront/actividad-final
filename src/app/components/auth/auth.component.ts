import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, timeInterval, timestamp } from 'rxjs';
import { ProductosService } from '../dashboard/products/products.service';
import { AuthResponseData, AuthService } from './auth.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  formAuth: FormGroup;
  hide = true;
  loading = false;


  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar,
    private router: Router,
    private auth: AuthService,private Ps:ProductosService) {

    this.formAuth = this.fb.group({
      user: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]

    })
  }

  ngOnInit(): void {
  }


  alert(mesage: string) {
    this._snackBar.open(mesage, '',
      {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      })
  }
  
  Submit() {

    if (!this.formAuth.valid) {
      return;
    }

    let authObs: Observable<AuthResponseData>;
    const user = this.formAuth.value.user;
    const password = this.formAuth.value.password;

    this.loading = true;

    authObs = this.auth.Login(user, password);

    authObs.subscribe(
      (resData: any) => {
        if (resData.length > 0) {
          setTimeout(() => {
            this.loading = false;
            this.router.navigate(['dashboard']);
            this.alert('¡¡¡ Bienvenido');
           
          }, 1000);
        } else {
          setTimeout(() => {
            this.alert('¡¡¡ Usuario o contraseña no validas');
            this.loading = false;
          }, 1000);

        }
      }
    );
    this.formAuth.reset()
  }

}
