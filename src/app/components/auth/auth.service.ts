import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { Router } from '@angular/router';
import { ProductosService } from '../dashboard/products/products.service';
import { environment } from 'src/environments/environment';


export interface AuthResponseData {
  id: number;
  email: string;
  usuario: string;
  password: string;
  perfil: string;
  auth?: boolean;
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  user = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient, 
    private router: Router,
    private prodctService:ProductosService,
    private productServi:ProductosService) { }

  Login(email: string, password: string) {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('email', email);
    searchParams = searchParams.append('pass', password);
    return this.http.get<AuthResponseData>(environment.API_URL_USUARIOS,
      { params: searchParams }
    )
      .pipe(
        tap((resDate: any) => {
          for (let index = 0; index < resDate.length; index++) {

            this.handleAuthentication(
              resDate[index]['email'],
              resDate[index]['usuario'],
              resDate[index]['pass'],
              resDate[index]['perfil'],
              resDate[index]['id']
            );
          }
        })
      );
  }

  private handleAuthentication(
    email: string,
    usuario: string,
    password: string,
    perfil: string,
    _id: string,
  ) {
    const expirationDate = new Date(new Date().getTime() * 1000);
    const user = new User(email, usuario, password, perfil, _id, expirationDate);
    this.user.next(user);
    localStorage.setItem('userData', JSON.stringify(user));
    this.productServi.getProductsListByUrl();

  }

  logout() {
    this.user.next(null)
    this.router.navigate(['/logout']);
    this.prodctService.logOut();
    localStorage.removeItem('userData');
  }

  autoLogin() {
    this.productServi.getProductsListByUrl();
    const userData: {
      email: string;
      usuario: string;
      password: string;
      perfil: string;
      _id: string;
      _ExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return;
    }
 
    const loadedUser = new User(userData.email,
      userData.usuario,
      userData.password,
      userData.perfil, userData._id,
      new Date(userData._ExpirationDate));
    if (loadedUser.token) {
      this.user.next(loadedUser);
    }
  }
}
