export class User {

  constructor(
    public email: string,
    public usuario: string,
    public password: string,
    public perfil: string,
    private _id: string,
    private _ExpirationDate: Date
  ) { }

  get token() {
    if (!this._ExpirationDate || new Date() > this._ExpirationDate) {
      return null
    }
    return this._id
  }

}
