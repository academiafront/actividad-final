import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { ProductNewComponent } from './products/product-new/product-new.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: '', component: InicioComponent, canActivate: [AuthGuard] },
      {
        path: 'productos/crear-producto', component: ProductNewComponent, canActivate: [AuthGuard], children: [
          { path: ':id', component: ProductNewComponent },
          { path: ':id/edit', component: ProductNewComponent },
        ]
      },
      { path: 'productos', loadChildren: () => import('../dashboard/products/products.module').then(x => x.ProductsModule) },
    ]

  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
