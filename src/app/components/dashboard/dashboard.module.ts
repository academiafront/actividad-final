import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
/* import { ProductosComponent } from './productos/productos.component'; */
import { InicioComponent } from './inicio/inicio.component';
/* import { ProductosListComponent } from './productos/productos-list/productos-list.component';
import { ProductItemComponent } from './productos/productos-list/product-item/product-item.component';

 */
@NgModule({
  declarations: [
    DashboardComponent,
    NavbarComponent,
/*     ProductosComponent, */
    InicioComponent,
/*     ProductosListComponent,
    ProductItemComponent */
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule
  ]
})
export class DashboardModule { }
