import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  isAuthenticated = false;
  perfil: string = '';
  userName: string = '';

  private userSub: Subscription;


  constructor(private authSEr: AuthService) { }

  ngOnInit(): void {

    this.userSub = this.authSEr.user.subscribe(user => {
      this.isAuthenticated = !!user;
      /*  */
    });
    this.getUserSetings()
  }
  ngOnDestroy(): void {
    this.userSub.unsubscribe();

  }
  Onlogaut() {
    this.authSEr.logout();

  }
  
  getUserSetings() {
    const userData: {
      email: string;
      usuario: string;
      password: string;
      perfil: string;
      _id: string;
      _ExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData'));
    this.perfil = userData.perfil;
    this.userName = userData.usuario;
  }
}
