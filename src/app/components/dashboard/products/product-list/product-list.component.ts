import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../product.model';
import { ProductosService } from '../products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @Input() Permisions: boolean;

  listProducts: Product[];
  displayedColumns: string[] = ['name', 'product_info', 'stock', 'price', 'accions'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private pService: ProductosService) { }

  ngOnInit(): void {
    this.getProducts();
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteProduct(idProduct: number) {
    this.pService.deleteProduct(idProduct.toString());
    this.getProducts();
  }

  getProducts() {
    this.listProducts = this.pService.getProducts();
    this.dataSource = new MatTableDataSource(this.listProducts);
    this.pService.productChanged.subscribe((product: Product[]) => {
      this.listProducts = product;
    })
  }
}
