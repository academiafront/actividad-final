import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ProductosService } from '../products.service';
import { Product } from '../product.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.css']
})
export class ProductNewComponent implements OnInit {

  formProduct: FormGroup;
  id: string;
  editMode = false;

  imagePath: string = './assets/img/nuevo-producto.png';

  constructor(private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private Ps: ProductosService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {

    this.route.queryParams
      .subscribe(params => {
        this.id = params['id'];
        this.editMode = params['id'] != undefined;
        this.initForm();
      }
      );
  }

  private initForm() {
    let nameProduct = '';
    let descripction = '';
    let brand = '';
    let imagePath = '';
    let stockProduct = 0;
    let priceProduct = 0;
    let idProduct = '';

    if (this.editMode) {
      const product = this.Ps.getProductById(this.id);
      nameProduct = product.nombre_producto;
      descripction = product.descipcion;
      brand = product.marca;
      imagePath = product.img;
      stockProduct = product.stock;
      priceProduct = product.precio;
      this.imagePath = product.img;
    }

    this.formProduct = this.fb.group({
      idProduct: [idProduct],
      nameProduct: [nameProduct, Validators.required],
      descripction: [descripction, Validators.required],
      brand: [brand, Validators.required],
      imagePath: [imagePath, Validators.required],
      priceProduct: [priceProduct, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]],
      stockProduct: [stockProduct, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]],
    })
  }

  Submit() {
    //creamos el id apartir d eun numero aleatorio entre 1 a 10 y le concatenamos el nombre del producto
    let id = Math.round(Math.random() * 10);
    let idProducto;
    if (!this.editMode) {
      idProducto = id + '_' + this.formProduct.value['nameProduct'];
    } else {
      idProducto = this.formProduct.value['idProduct']
    }
    //se crearon las variables ya que los names no son iguales que en la base si fuera asi se mandan directos
    let nombreProducto = this.formProduct.value['nameProduct'];
    let marca = this.formProduct.value['brand'];
    let precio = this.formProduct.value['priceProduct'];
    let descipcion = this.formProduct.value['descripction'];
    let stock = this.formProduct.value['stockProduct'];
    let img = this.formProduct.value['imagePath'];

    const newProduct = new Product(idProducto, nombreProducto, marca, precio, descipcion, stock, img);

    if (this.editMode) {
      this.Ps.updateProduct(this.id, newProduct);
      this.alert('Exito: El producto ha sido editado');
    } else {
      this.Ps.addProduct(newProduct);
      this.alert('Exito: El producto ha sido creado');
    }
    this.onCancel();
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  alert(mesage: string) {
    this._snackBar.open(mesage, '',
      {
        duration: 1000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      })
  }
}
