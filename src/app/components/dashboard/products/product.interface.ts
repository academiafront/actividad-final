export interface Producto {
    id: string;
    nombre_producto: string;
    marca: string;
    precio: number;
    descipcion: string;
    stock: number;
    img: string;
  }
  