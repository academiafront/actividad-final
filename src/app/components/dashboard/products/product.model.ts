export class Product {

    constructor(
        
        public id: string,
        public nombre_producto: string,
        public marca: string,
        public precio: number,
        public descipcion: string,
        public stock: number,
        public img: string) {

    }
}
