import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductNewComponent } from './product-new/product-new.component';
import { ProductsComponent } from './products.component';


const routes: Routes = [
  {
    path: '', component: ProductsComponent, children: [
    { path: ':id', component: ProductNewComponent },
    { path: ':id/edit', component: ProductNewComponent },
    ]
  }

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
