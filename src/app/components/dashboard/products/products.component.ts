import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { DataStorageService } from '../../../shared/data-storage.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {

  hasPermisions = false;
  private userSub: Subscription;

  constructor(private dataStore: DataStorageService
    , private router: Router) { }

  ngOnInit(): void {
    this.getUserSetings();

  }
  newProduct() {
    this.router.navigate(['/dashboard/productos/crear-producto']);
  }
  ngOnDestroy(): void {

  }
  getUserSetings() {

    const userData: {
      email: string;
      usuario: string;
      password: string;
      perfil: string;
      _id: string;
      _ExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData'));
    userData.perfil === 'seller' ? this.hasPermisions = false : this.hasPermisions = true;

  }
}
