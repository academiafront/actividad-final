import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from './product.model';
import { map, Subject, tap } from 'rxjs';
import { Producto } from './product.interface';
import { DataStorageService } from '../../../shared/data-storage.service';
import { environment } from '../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  productChanged = new Subject<Product[]>();

  constructor(private http: HttpClient,
    private dataSer: DataStorageService) { }


  private listProducts: Product[] = [
  ];

  Producto: Producto = {
    id: '',
    nombre_producto: '',
    marca: '',
    precio: 0,
    descipcion: '',
    stock: 0,
    img: '',
  }

   getProductsListByUrl() {
    this.http.get<Product[]>(environment.API_URL_PRODUCTOS).subscribe(
      resDate => {
      this.setProduct(resDate);
    })
  } 

  setProduct(Products: Product[]) {
    this.listProducts = Products;
    this.productChanged.next(this.listProducts.slice());
  }


  getProducts() {
    return this.listProducts.slice();
  }


  addProduct(product: Product) {
    this.dataSer.storeProducts(product)
    this.listProducts.push(product);
    this.productChanged.next(this.listProducts.slice());
  }

  getProductById(idProduct: string) {

    const indice = this.getIndexOfProductList(idProduct)
    return this.listProducts[indice];
  }


  updateProduct(idProduct: string, newProduct: Product) {
    const indice = this.getIndexOfProductList(idProduct);
    this.listProducts[indice] = newProduct;
    this.productChanged.next(this.listProducts.slice());
  }

  deleteProduct(idProduct: string) {
    const indice = this.getIndexOfProductList(idProduct);
    this.dataSer.deleteProducts(idProduct);
    this.listProducts.splice(indice, 1);
    this.productChanged.next(this.listProducts.slice());

  }

  logOut() {
    this.listProducts = [];
  }

  getIndexOfProductList(index: string) {
    const indice = this.listProducts.findIndex((elemento, indice) => {
      if (elemento.id === index) {
        return true;
      }
    });
    return indice
  }

}
