import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../components/dashboard/products/product.model';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private http: HttpClient) { }

  storeProducts(product: Product) {
    this.http
      .post(
        environment.API_URL_PRODUCTOS,
        product
      )
      .subscribe(response => {
      });
  }


  deleteProducts(idProduct: string) {
    this.http.delete(environment.API_URL_PRODUCTOS + idProduct).
      subscribe(
        response => {
        }
      );
  }

  updateProduct(product: Product){
    this.http.patch(
      environment.API_URL_PRODUCTOS +product.id,
      product
    )
    .subscribe(response => {
      console.log(response)
    });
  }

}
