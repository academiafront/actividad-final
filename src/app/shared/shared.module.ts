import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

//modulos
import { ReactiveFormsModule } from '@angular/forms';

//Angular Material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar'; 
import {MatTableModule} from '@angular/material/table'; 
import {MatCardModule} from '@angular/material/card'; 
import {MatTooltipModule} from '@angular/material/tooltip'; 
import {MatPaginatorModule} from '@angular/material/paginator'; 
import {MatSortModule} from '@angular/material/sort'; 
import {MatGridListModule} from '@angular/material/grid-list'; 

@NgModule({
    declarations: [],
    imports: [CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        MatToolbarModule,
        MatTableModule,
        MatCardModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatSortModule,
        MatGridListModule
    ],
    exports:[
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        MatToolbarModule,
        MatTableModule,
        MatCardModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatSortModule,
        MatGridListModule
    ]
})
export class SharedModule { }