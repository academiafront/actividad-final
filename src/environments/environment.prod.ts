export const environment = {
  production: true,
  API_URL_USUARIOS: 'http://localhost:3000/usuarios',
  API_URL_PRODUCTOS: 'http://localhost:3000/productos'
};
